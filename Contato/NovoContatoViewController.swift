//
//  NovoContatoViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

protocol NovoContatoViewControllerDelegate {
    func salvarNovoContao(contato: Contato)
    }

class NovoContatoViewController: UIViewController {

    
    @IBOutlet weak var txtNome: UITextField!
    @IBOutlet weak var txtNumero: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txEndereco: UITextField!
    
    
    public var delegate: NovoContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func SalvarContato(_ sender: Any) {
        
        let contato = Contato(nome: txtNome?.text ?? "",
                              email: txtEmail?.text ?? "",
                              endereco: txEndereco?.text ?? "",
                              telefone: txtNumero?.text ?? "")
        
        delegate?.salvarNovoContao(contato: contato)
        navigationController?.popViewController(animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ViewController: NovoContatoViewControllerDelegate{
    func salvarNovoContao(contato: Contato) {
        listaDeContatos.append(contato)
        tableview.reloadData()
    }
}
