//
//  DetalhesViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

class DetalhesViewController: UIViewController {

    @IBOutlet var labelNome: UILabel!
    @IBOutlet var labelNumero: UILabel!
    @IBOutlet var labelEmail: UILabel!
    @IBOutlet var labelEndereco: UILabel!
    
    public var contato: Contato?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = contato?.nome
        
        labelNome.text = contato?.nome
        labelNumero.text = contato?.telefone
        labelEmail.text = contato?.email
        labelEndereco.text = contato?.endereco

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
